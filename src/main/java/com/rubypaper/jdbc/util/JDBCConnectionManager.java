package com.rubypaper.jdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBCConnectionManager {
	private String driverClass ; 
	private String url; 
	private String username ;
	private String password;
	public String getDriverClass() {
		return driverClass;
	}
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsrname() {
		return username;
	}
	public void setUsrname(String usrname) {
		this.username = usrname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Connection getConnetction () {
		try {
				Class.forName(driverClass);
				return DriverManager.getConnection(url,username , password) ;
		}catch(Exception e ) {
			e.printStackTrace();
		}
		return null;
		
	}
	@Override 
	public String toString() {
		return "[DriverClass]"+driverClass + "[url]"+url;
	}
	
}
